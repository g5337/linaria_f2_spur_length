#!/bin/bash
#SBATCH -J bam_merge_job
#SBATCH -A bioinf
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --time=001:00:00
#SBATCH --mem=10G
#SBATCH --mail-type=END,FAIL
#SBATCH --no-requeue
#SBATCH -p production
#SBATCH -o bam_merge_%j.out
#SBATCH -e bam_merge_%j.err

# Set the threads argument
threads=8

# Set the input and output file paths
bam_folder=~/spurs/results/alignment/sorted_bams_2
output_folder=~/spurs/results/alignment/merged_bams

# Create the output folder if it doesn't exist
mkdir -p "$output_folder"

# Specify the input and output file names
input1="$bam_folder/B801_E100_sorted.bam"
input2="$bam_folder/B801_E150_sorted.bam"
output1="$output_folder/B801_sorted.bam"

# Perform the first merge
samtools merge -@ "$threads" -f "$output1" "$input1" "$input2"

# Specify the input and output file names
input3="$bam_folder/B1247_E100_sorted.bam"
input4="$bam_folder/B1247_E150_sorted.bam"
output2="$output_folder/B1247_sorted.bam"

# Perform the second merge
samtools merge -@ "$threads" -f "$output2" "$input3" "$input4"






# Did the other merge manually first using samtools merge -f B734_sorted.bam B734_E100_sorted.bam B734_E150_sorted.bam
# Ran flagstat commands:
#
#samtools flagstat B734_sorted.bam > B734_sorted.bam.flagstat for each new bam file
#Checked total read number in final flagstat file - made sure it was the same as the unmerged flagstats combined.

