####################################################
# STEP 0: Set up                                   #
####################################################

# GATK
GATK_BIN='~/bin/gatk-4.2.6.1/gatk --java-options "-Xmx8g"'

# reference genome
reference_fasta=~/linaria_f2_spur_length/temp/reference/ncbi_dataset/data/GCA_948329865.1/GCA_948329865.1_daLinVulg1.1_genomic.fna

# working directory
WORK_DIR=/home/qw254/linaria_f2_spur_length/temp/vcf

cd ${WORK_DIR}

# input and output files

file_tag=Lb_Lc

merged_gvcf_file=${file_tag}.g.vcf.gz
genotype_call_file=${file_tag}.variants.vcf.gz



#########################################################################
# Option B use GenomicsDB(https://github.com/GenomicsDB/GenomicsDB/wiki)
# which is supposed to be more efficient, still testing
#########################################################################


#############################################################
# option A STEP 1: combine GVCF files from multiple samples #
#############################################################

# Note: the order of the samples in the output vcf is the same as the input?
# For this one, the resulting order in vcf is Lb1   Lb2	Lc1	Lc2	Lc3	Lc4	Lc5	Lc6	Lc7	Lc8

# should consider use GenomicsDBImport instead of CombineGVCFs, according to
# https://gatk.broadinstitute.org/hc/en-us/articles/360035535932-Germline-short-variant-discovery-SNPs-Indels-

# The bash line below helps to write the combind_gvcf_command:
# for x in *gz; do echo "--variant $x \\"; done

combind_gvcf_command="${GATK_BIN} CombineGVCFs \
   -R ${reference_fasta} \
    --variant Lb1_OX415249.1.vcf.gz \
    --variant Lb2_OX415249.1.vcf.gz \
    --variant Lc1_OX415249.1.vcf.gz \
    --variant Lc2_OX415249.1.vcf.gz \
    --variant Lc3_OX415249.1.vcf.gz \
    --variant Lc4_OX415249.1.vcf.gz \
    --variant Lc5_OX415249.1.vcf.gz \
    --variant Lc6_OX415249.1.vcf.gz \
    --variant Lc7_OX415249.1.vcf.gz \
    --variant Lc8_OX415249.1.vcf.gz \
    -O ${merged_gvcf_file}"

# only run if it the output does not exist
if [ ! -e ${merged_gvcf_file} ] 
    then 
    eval ${combind_gvcf_command}
fi


####################################################
# Option A STEP 2 : Variant calling of multiple samples      #
####################################################
# https://gatk.broadinstitute.org/hc/en-us/articles/5358906861083-GenotypeGVCFs

genotype_command="${GATK_BIN} GenotypeGVCFs \
    -R ${reference_fasta} \
    -V ${merged_gvcf_file} \
    -O ${genotype_call_file}"

# only run if it the output does not exist
if [ ! -e ${genotype_call_file} ] 
    then 
    eval ${genotype_command}
fi

#############################################################
# option B STEP 1 : combine GVCF files from multiple samples #
#############################################################
# Note: the order of the samples in the output vcf is the same as the input?
# For this one, the resulting order in vcf is Lb1   Lb2	Lc1	Lc2	Lc3	Lc4	Lc5	Lc6	Lc7	Lc8

# should consider use GenomicsDBImport instead of CombineGVCFs, according to
# https://gatk.broadinstitute.org/hc/en-us/articles/360035535932-Germline-short-variant-discovery-SNPs-Indels-

# The bash line below helps to write the combind_gvcf_command:
# for x in *gz; do echo "--variant $x \\"; done

# https://gatk.broadinstitute.org/hc/en-us/articles/360035889971--How-to-Consolidate-GVCFs-for-joint-calling-with-GenotypeGVCFs

genomics_database=${WORK_DIR}/gatk_database
TMP_DIR=/local_data/qw254
region=OX415249.1
genotype_call_file_db=${file_tag}.db.variants.vcf.gz

combind_gvcf_command_B="${GATK_BIN} GenomicsDBImport \
    -R ${reference_fasta} \
    --variant Lb1_OX415249.1.vcf.gz \
    --variant Lb2_OX415249.1.vcf.gz \
    --variant Lc1_OX415249.1.vcf.gz \
    --variant Lc2_OX415249.1.vcf.gz \
    --variant Lc3_OX415249.1.vcf.gz \
    --variant Lc4_OX415249.1.vcf.gz \
    --variant Lc5_OX415249.1.vcf.gz \
    --variant Lc6_OX415249.1.vcf.gz \
    --variant Lc7_OX415249.1.vcf.gz \
    --variant Lc8_OX415249.1.vcf.gz \
    --genomicsdb-workspace-path ${genomics_database} \
    --tmp-dir ${TMP_DIR} \
    -L ${region}"

eval ${combind_gvcf_command_B}

##############################################################
# Option B STEP 2 : Variant calling of multiple samples      #
##############################################################

# https://gatk.broadinstitute.org/hc/en-us/articles/5358906861083-GenotypeGVCFs

genotype_db_command="${GATK_BIN} GenotypeGVCFs \
    -R ${reference_fasta} \
    -V gendb://${genomics_database} \
    -O ${genotype_call_file_db}
    --tmp-dir ${TPM_DIR}"

# only run if it the output does not exist
if [ ! -e ${genotype_call_file_db} ] 
    then 
    eval ${genotype_db_command}
fi

########################################################
# STEP 3: Tag variant for filter variant calling later #
########################################################
# filter variant calling
# reference:
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531112--How-to-Filter-variants-either-with-VQSR-or-by-hard-filtering#2

# reference for filtering criteria:
# https://gatk.broadinstitute.org/hc/en-us/articles/360035891011-JEXL-filtering-expressions

# explanation of what all variant calls mean
# https://gatk.broadinstitute.org/hc/en-us/articles/13832655155099--Tool-Documentation-Index#VariantAnnotations

# TODO: make all paraters as variable for easier adjustment

filter_name='"test_filter"'
filter_tag="test_filter" # for filename

filter_opt='" QD < 10.0 || FS > 10.0 || DP < 30.0 || QUAL < 30.0 "'
#filter_opt='" QD < 10.0 || FS > 10.0 || DP < 30.0 || QUAL < 30.0 "'
#zcat Lb_Lc.variants.filtered.test_filter.vcf.gz | awk '$7=="PASS"' | wc -l
#2,744,432

filtered_genotype_call_file=${file_tag}.variants.filtered.${filter_tag}.vcf.gz

filter_command="${GATK_BIN} VariantFiltration \
    -R ${reference_fasta} \
    -V ${genotype_call_file} \
    -O ${filtered_genotype_call_file} \
    --filter-expression ${filter_opt} \
    --filter-name ${filter_name} "

# only run if it the output does not exist
#if [ ! -e ${filtered_genotype_call_file} ]
#    then 
    eval ${filter_command}
#fi

# filter_opt='"DP < 30"'
# (base) qw254@hydrogen:~/linaria_f2_spur_length/temp/vcf$ zcat Lb_Lc.variants.filtered.test_filter.vcf.gz | awk '$7=="PASS"' | wc -l
# 3,083,498
# (base) qw254@hydrogen:~/linaria_f2_spur_length/temp/vcf$ zcat Lb_Lc.variants.filtered.test_filter.vcf.gz | wc -l
# 4,590,474

# VCF example line
# OX415249.1	64174	.	C	<NON_REF>	.	.	END=64180	GT:DP:GQ:MIN_DP:PL	0/0:5:12:5:0,12,180

# more info on VCF format
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531692-VCF-Variant-Call-Format

# NON_REF: "Represents any possible alternative allele not already represented at this location by REF and ALT"
# LowQual:"Low quality"

# GT:"Genotype"
# DP:"Approximate read depth (reads with MQ=255 or with bad mates are filtered)"
# GQ:"Genotype Quality". GQ is the difference between the PL of the second most likely genotype, and the PL of the most likely genotype. 
# MIN_DP:"Minimum DP observed within the GVCF block"
# PL:"Normalized, Phred-scaled likelihoods for genotypes (0/0, 0/1, and 1/1). The PL values are "normalized" so that the PL of the most likely genotype (assigned in the GT field) is 0 in the Phred scale. 
# AD:"Allelic depths for the ref and alt alleles in the order listed">

# PGT:"Physical phasing haplotype information, describing how the alternate alleles are phased in relation to one another; will always be heterozygous and is not intended to describe called alleles">
# PID: "Physical phasing ID information, where each unique ID within a given sample (but not across samples) connects records within a phasing group">
# PS:"Phasing set (typically the position of the first variant in the set)">
# SB: "Per-sample component statistics which comprise the Fisher's Exact Test to detect strand bias.">
